MODEL SMALL
STACK 100h
LOCALS @@


    .data
    some_x dw 10h
    .code

main label near
    mov ax, @data
    mov ds, ax

do:
        shl some_x, 1
        mov dx, some_x
        sub dx, 10h
        sub some_x, dx
    cmp some_x, 512
    jb do
ext:
    mov ah, 4ch
    mov al, 0
    int 21h
end main

MODEL SMALL
STACK 100h

    .data
    some_array db 100 dup (0)
    size_array = $-some_array
    .code

main label near
    mov ax, @data
    mov ds, ax
    xor ax, ax
    xor cx, cx
for_cycle:
    cmp cx, size_array
    jge after
        mov si, cx
        add ax, size_array[si]
    inc cx
    jmp for_cycle
after:

ext:
    mov ah, 4ch
    mov al, 0
    int 21h
end main

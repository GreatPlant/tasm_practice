MODEL SMALL
STACK 100h
LOCALS @@
    .data
    str_hex db "2bA", 0
    .code
;======================================
; translate hex string to hex number
; input:
; si - string with hex number
; output:
; ax - hex number
;======================================
str_to_int16 proc c near uses cx dx
    xor ax, ax
    mov cl, 4
@@lp:
    mov dl, [si]
    cmp dl, 0
    je  @@ext
        shl ax, cl
        cmp dl, '0'
        jb  @@err
        cmp dl, '9'
        ja  @@alpha
        sub dl, 30h
        jmp @@next
    @@alpha:
        or  dl, 20h
        cmp dl, 'a'
        jb  @@err
        cmp dl, 'f'
        ja  @@err
        sub dl, 'a'- 10
    @@next:
        add al, dl
    inc si
    jmp @@lp
@@err:
    stc
    ret
@@ext:
    clc
    ret
str_to_int16 endp

main label near
    mov ax, @data
    mov ds, ax
    mov si, offset str_hex
    call str_to_int16
ext:
    mov ah, 4ch
    mov al, 0
    int 21h
end main

model small
stack 256
    .data
	N = 5
	mas db 5 dup (3 dup(0))
    .code
main:
	mov ax,@data
	mov ds,ax

	mov si, N*3-3
	mov cx, N
    xor dh, dh
go:
	mov dl,mas[si]
	inc dl
	push dx
    sub si, 3
loop go
	mov cx, N
show:
	pop dx
	add dl, 30h
	mov ah, 02h
	int 21h
loop show
exit:
	mov ax,4c00h
	int 21h
end main

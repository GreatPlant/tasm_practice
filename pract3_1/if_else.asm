MODEL SMALL
STACK 100h

    .data
    some_x db 15
    first_value equ 0
    second_value equ 10
    third_value equ 100
    .code

main label near
    mov ax, @data
    mov ds, ax
    ;first variant if...else
    cmp some_x, first_value
    jne scd
        ;work if some_x = 0
scd:
    cmp some_x, second_value
    jne thrd
        ;work if some_x = 10
thrd:
    cmp some_x, third_value
    jne default
        ;work if some_x = 100
default:
        ;default work with some_x
ext:
    mov ah, 4ch
    mov al, 0
    int 21h
end main

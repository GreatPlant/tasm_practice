
MODEL SMALL
STACK 100h
LOCALS @@


    .data
    labels dw case1, case2, case3
    n_of_labels db $-labels
    var db 2
    .code

main label near
    mov ax, @data
    mov ds, ax
    ;second variant with label's table
    cmp var, 0
    jb  default
    cmp var, 2
    ja  default
    mov bx, var

    jmp addrs[bx*2]
case1:
    ;work if some_x = 0
    jmp ext_case
case2:
    ;work if some_x = 10
    jmp ext_case
case3:
    ;work if some_x = 100
    jmp ext_case
default:
        ;default work with some_x

ext:
    mov ah, 4ch
    mov al, 0
    int 21h
end main

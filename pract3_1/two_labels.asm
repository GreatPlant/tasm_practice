
MODEL SMALL
STACK 100h
LOCALS @@


    .data
    ; labels array
    labels dw case1, case2, case3
    n_of_labels = $-labels
    ; values arrayd
    values db 0, 10, 100
    n_of_values = $-values
    ; variable x
    some_x db 10
    .code

main label near
    mov ax, @data
    mov ds, ax
    ;third variant with two tables
    cld
    mov cx, n_of_values
    lea di, values
    mov al, some_x
    repne scasb
    jne default
    ;������让 ��� ��� ��宦����� ������ ��⪨
    ;����砫쭮 cx �� ࠢ�� ����� ���ᨢ� n
    ;��᫥ scasb �� �⠫ ࠢ��  n - x ������⮢
    ;�᪮�� ������ - x
    sub cx, n_of_values ;��室�� ࠧ����� � ������ ���ᨢ� (n-x-n) = -n
    not cx  ;�����頥� ������⥫�� ���� !(-n) = n
    mov bx, cx

    jmp addrs[bx*2]
case1:
    ;work if some_x = 0
    jmp ext_case
case2:
    ;work if some_x = 10
    jmp ext_case
case3:
    ;work if some_x = 100
    jmp ext_case
default:
        ;default work with some_x

ext:
    mov ah, 4ch
    mov al, 0
    int 21h
end main
